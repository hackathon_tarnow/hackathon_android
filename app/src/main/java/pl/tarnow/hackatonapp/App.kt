package pl.tarnow.hackatonapp

import android.app.Application
import android.support.multidex.MultiDexApplication
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import pl.tarnow.hackatonapp.api.apiModule
import timber.log.Timber

class MyApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        Timber.plant(Timber.DebugTree())

        startKoin {
            androidContext(this@MyApp)
            modules(apiModule)
        }
    }
}