package pl.tarnow.hackatonapp.ui.news.list

import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_news.*
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import pl.tarnow.hackatonapp.R
import pl.tarnow.hackatonapp.ui.KtxDelegate
import pl.tarnow.hackatonapp.ui.KtxHolder

data class NewsItem(
    val id: Long,
    val name: String,
    val imageUrl: String,
    val date: LocalDateTime
)

class NewsDelegate(
    private val listener: Listener
) : KtxDelegate<NewsItem>(NewsItem::class.java, R.layout.item_news) {
    private val dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")

    override fun onBindViewHolder(holder: KtxHolder, item: NewsItem, payloads: List<Any>) {
        holder.name.text = item.name
        holder.date.text = item.date.format(dateFormatter)
        Picasso.Builder(holder.thumbnail.context)
            .listener { _, _, e -> e.printStackTrace() }
            .build()
            .load(item.imageUrl)
            .error(R.drawable.image_placholder)
            .fit()
            .into(holder.thumbnail)

        holder.itemView.setOnClickListener { listener.onNewsClick(item) }
    }

    interface Listener {
        fun onNewsClick(item: NewsItem)
    }
}