package pl.tarnow.hackatonapp.ui.denunciation

import io.reactivex.android.schedulers.AndroidSchedulers
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.api.PostReportBody
import pl.tarnow.hackatonapp.ui.Presenter
import timber.log.Timber

interface DenunciationView {
    fun showSuccess()
    fun showError()
}

class DenunciationPresenter(
    private val api: Api

) : Presenter<DenunciationView>() {

    fun send(title: String, info: String) {
        api.postReport(PostReportBody(title = title, application = info))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { view.showSuccess() },
                {
                    Timber.e(it)
                    view.showError()
                }
            )
            .also { disposables.add(it) }
    }
}