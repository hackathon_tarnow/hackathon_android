package pl.tarnow.hackatonapp.ui.events.details


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_events_details.*
import org.koin.android.ext.android.inject
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

import pl.tarnow.hackatonapp.R
import pl.tarnow.hackatonapp.api.Api
import android.content.Intent
import android.net.Uri
import org.koin.core.qualifier.named


class EventDetailsFragment : Fragment(), EvensDetailsView {

    private val api: Api by inject()
    private val baseurl: String by inject(named("BASEURL"))
    private val presenter = EventDetailsPresenter(api, baseurl)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_events_details, container, false)
    }

    private val dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener {
            NavHostFragment.findNavController(this).popBackStack()
        }

        mapFab.setOnClickListener {
          presenter.onMapFabClick()
        }
    }

    override fun showError() {
        Toast.makeText(context, getString(R.string.generic_error), Toast.LENGTH_LONG).show()
    }

    override fun showEvents(imageUrl: String, name: String, date: LocalDateTime, details: String) {
        this.name.text = name
        this.date.text = date.format(dateFormatter)
        this.details.text = details

        Picasso.Builder(context)
            .listener { _, _, e -> e.printStackTrace() }
            .build()
            .load(imageUrl)
            .error(R.drawable.image_placholder)
            .fit()
            .into(image)

        mapFab.visibility = View.VISIBLE
    }

    override fun onStart() {
        super.onStart()
        presenter.onAttach(this)
        presenter.loadEventDetails(arguments!!.getInt("eventId").toLong())
    }

    override fun onStop() {
        presenter.onDetach()
        super.onStop()
    }

    override fun openMap(latitude: Double, longitude: Double) {
        val intent = Intent(
            android.content.Intent.ACTION_VIEW,
            Uri.parse("geo:$latitude,$longitude")
        )
        startActivity(intent)
    }
}
