package pl.tarnow.hackatonapp.ui.start


import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import com.google.zxing.client.android.CaptureActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.fragment_start.*
import pl.tarnow.hackatonapp.R
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import timber.log.Timber
import android.widget.EditText
import android.support.v7.app.AlertDialog
import android.preference.PreferenceManager
import org.koin.android.ext.android.inject
import org.koin.core.qualifier.named


class StartFragment : Fragment() {

    private val currentApiUrl : String by inject(named("BASEURL"))

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        scanQrCode.setOnClickListener {
            RxPermissions(this)
                .request(Manifest.permission.CAMERA)
                .subscribe { granted ->
                    if (granted) {
                        val intent = Intent(context, CaptureActivity::class.java)
                        intent.action = "com.google.zxing.client.android.SCAN"
                        intent.putExtra("SAVE_HISTORY", false)
                        startActivityForResult(intent, 0)
                    }
                }
        }

        val navController = NavHostFragment.findNavController(this)

        newsButton.setOnClickListener {
            navController.navigate(R.id.action_startFragment_to_newsListFragment)
        }

        placesButton.setOnClickListener {
            navController.navigate(R.id.action_startFragment_to_placesFragment)
        }

        eventsButton.setOnClickListener {
            navController.navigate(R.id.action_startFragment_to_eventsListFragment)
        }

        denunciationButton.setOnClickListener {
            navController.navigate(R.id.action_startFragment_to_reportsListFragment)
        }

        toolbar.setNavigationOnClickListener {
            navController.navigate(R.id.action_startFragment_to_searchFragment)
        }

        settings.setOnClickListener {
            val apiUrl = EditText(context)
            apiUrl.setText(currentApiUrl)

            val prefs = PreferenceManager.getDefaultSharedPreferences(activity!!.applicationContext)

            AlertDialog.Builder(context!!)
                .setTitle("API BASEURL")
                .setView(apiUrl)
                .setPositiveButton("SET") { _, _ ->
                    prefs.edit().putString("url", apiUrl.text.toString()).commit()
                }
                .setNegativeButton("CANCEL") { _, _ -> }
                .show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                val contents = data!!.getStringExtra("SCAN_RESULT")
                Timber.d("contents: $contents")
                handleQrCode(contents)
            } else if (resultCode == RESULT_CANCELED) {
                Timber.d("RESULT_CANCELED")
            }
        }
    }

    private fun handleQrCode(contents: String) {
        try {
            when {
                contents.startsWith("PLACE:") -> {
                    val id = contents.substringAfter(delimiter = ":").toLong()

                    val bundl = Bundle().apply {
                        putLong("selectedId", id)
                    }
                    NavHostFragment.findNavController(this).navigate(R.id.action_startFragment_to_placesFragment, bundl)
                }

            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }
}
