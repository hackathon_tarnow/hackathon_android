package pl.tarnow.hackatonapp.ui.news.details

import io.reactivex.android.schedulers.AndroidSchedulers
import org.threeten.bp.LocalDateTime
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.ui.Presenter
import timber.log.Timber

interface NewsDetailsView {
    fun showNews(imageUrl: String, name: String, date: LocalDateTime, details: String)
    fun showError()
}

class NewsDetailsPresenter(
    private val api: Api,
    private val baseurl: String
) : Presenter<NewsDetailsView>() {

    fun loadNewsDetails(id: Long) {
        api.getNewsDetails(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { details ->
                    view.showNews(
                        imageUrl = "$baseurl/files/${details.fileName}",
                        name = details.title,
                        date = details.data,
                        details = details.text ?: ""
                    )
                },
                { error ->
                    Timber.e(error)
                    view.showError()
                }
            )
            .also { disposables.add(it) }


//        view.showNews(
//            imageUrl = "https://www.tarnow.pl/var/ezwebin_site/storage/images/_aliases/widen_900_filter/8/9/5/0/1240598-1-pol-PL/BIEGI%20PRZE%C5%81AJOWE%20MARCINKA%20FOT.%20ARTUR%20GAWLE%20(51).jpg",
//            name = "Przełajowe mistrzostwa na „Marcince”",
//            date = LocalDateTime.now(),
//            details = "W odbywających się pod patronatem prezydenta Tarnowa, Romana Ciepieli, Międzywojewódzkich Mistrzostw Młodzików wystartowało 74 młodych biegaczy (rocznik 2004 i młodsi) z Małopolski i Podkarpacia, rywalizujących na czterech dystansach.\n" +
//                    "\n" +
//                    "W biegu na 1500 metrów młodziczek najszybsza okazała się Alicja Magdoń z Resovii Rzeszów – 5.58 min, wyprzedzając Nikolę Nagięć z Wieliczanki – 6.03 min oraz Victorię Puk ze Stali Nowa Dęba – 6.08 min. Spośród zawodniczek gospodarzy najlepsza okazała się Oliwia Michoń z AZS PWSZ Tarnów, która z czasem 6.35 min, sklasyfikowana została na jedenastej pozycji. Rywalizacja na dystansie o kilometr dłuższym zakończyła się natomiast sukcesem Julii Makuch z LKS Brzezina Osiek – 10.34 min, w pokonanym polu zostawiając Gabrielę Urbańską ze Stali Nowa Dęba – 10.43 min oraz Magdalenę Ciupińską z UKS Tempo 5 Przemyśl – 10.47 min. Na siódmym miejscu linię mety minęła najszybsza z tarnowianek, Martyna Dzierwa (MLUKS Tarnów) – 11.39 min.\n" +
//                    "\n" +
//                    "Zmagania młodzików mających do pokonania dystans 2000 metrów zakończyły się sukcesem Adriana Marka z Wieliczanki – 7.29 min, za którego plecami linię mety minęli Piotr Ciężkowski z Cracovii 1906 Kraków – 7.32 min oraz Jakub Chudzik z UKS Tiki-Taka Kolbuszowa – 7.34 min. Jako ósmy na mecie zameldował się natomiast najlepszy zawodnik z naszego miasta, Miłosz Wróbel z MLUKS Tarnów – 8.02 min. Dystans 3000 metrów najszybciej pokonał Wiktor Zięba z UKS Tiki-Taka Kolbuszowa – 12.25 min, wyprzedzając Jana Mokrzyckiego ze Stali Nowa Dęba – 12.30 min oraz Adriana Pempusia z UKS Tempo 5 Przemyśl – 12.31 min. Jedyny startujący w tym biegu tarnowianin, Paweł Czapkowicz z AZS PWSZ Tarnów, z czasem 12.52 min uplasował się na piątej pozycji."
//
//        )
    }
}