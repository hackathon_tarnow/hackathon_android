package pl.tarnow.hackatonapp.ui.events.list

import io.reactivex.android.schedulers.AndroidSchedulers
import org.threeten.bp.LocalDateTime
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.ui.Presenter
import pl.tarnow.hackatonapp.ui.news.list.NewsItem

import timber.log.Timber

interface EventsListView {
    fun showEvents(events: List<NewsItem>)
    fun showError()
}

class NewsListPresenter(
    private val api: Api,
    private val baseurl: String
) : Presenter<EventsListView>() {

    fun loadNews() {

        api.getEvents()
            .map { eventsList ->
                eventsList.map { event ->
                    NewsItem(
                        id = event.id,
                        name = event.name,
                        imageUrl = "$baseurl/files/${event.fileName}",
                        date = event.eventDate
                    )
                }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { view.showEvents(it) },
                {
                    Timber.e(it)
                    view.showError()
                }
            )
            .also { disposables.add(it) }
    }
}