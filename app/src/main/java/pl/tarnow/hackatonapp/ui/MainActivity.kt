package pl.tarnow.hackatonapp.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import pl.tarnow.hackatonapp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
