package pl.tarnow.hackatonapp.ui.search


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import it.bienkowski.recyclerdelegates.DelegatingRecyclerAdapter
import it.bienkowski.recyclerdelegates.managers.SimpleDelegateManager
import kotlinx.android.synthetic.main.fragment_search.*
import org.koin.android.ext.android.inject
import pl.tarnow.hackatonapp.R
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.api.SearchResult
import timber.log.Timber


class SearchFragment : Fragment(), SearchView, SearchResultDelegate.Listener {


    private val api: Api by inject()
    private val presenter = SearchPresenter(api)

    private val adapter = DelegatingRecyclerAdapter<Any>(
        SimpleDelegateManager.withDelegates(
            SearchResultDelegate(this)
        )
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = NavHostFragment.findNavController(this)
        toolbar.setNavigationOnClickListener {
            navController.popBackStack()
        }

        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        searchField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                presenter.onQueryChange(p0.toString())
            }
        })
    }

    override fun showError() {
        Toast.makeText(context, getString(R.string.generic_error), Toast.LENGTH_LONG).show()
    }

    override fun showResults(items: List<SearchResult>) {
        Timber.i("ITEMS $items")
        adapter.submitList(items)
    }

    override fun setLoading(loading: Boolean) {
        if (loading) {
            loader.show()
        } else {
            loader.hide()
        }
    }

    override fun onSearchResultClick(item: SearchResult) {
        val navController = NavHostFragment.findNavController(this)
        navController.popBackStack()

        when (item.type) {
            "PLACE" -> {
                val bundl = Bundle().apply {
                    putLong("selectedId", item.objectId)
                }
                navController.navigate(R.id.action_startFragment_to_placesFragment, bundl)
            }

            "EVENT" -> {
                navController.navigate(R.id.action_startFragment_to_eventsListFragment)

                val args = Bundle().apply {
                    putInt("eventId", item.objectId.toInt())
                }
                navController.navigate(R.id.action_eventsListFragment_to_eventDetailsFragment, args)
            }

            "NEWS" -> {
                navController.navigate(R.id.action_startFragment_to_newsListFragment)

                val args = Bundle().apply {
                    putInt("newsId", item.objectId.toInt())
                }
                navController.navigate(R.id.action_newsListFragment_to_newsDetailsFragment, args)
            }

            "REPORT" -> {
                navController.navigate(R.id.action_startFragment_to_reportsListFragment)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.onAttach(this)
    }

    override fun onStop() {
        presenter.onDetach()
        super.onStop()
    }
}
