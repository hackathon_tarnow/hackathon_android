package pl.tarnow.hackatonapp.ui.search

import kotlinx.android.synthetic.main.item_search_result.*
import pl.tarnow.hackatonapp.R
import pl.tarnow.hackatonapp.api.SearchResult
import pl.tarnow.hackatonapp.ui.KtxDelegate
import pl.tarnow.hackatonapp.ui.KtxHolder

class SearchResultDelegate(
    private val listener: Listener
) : KtxDelegate<SearchResult>(SearchResult::class.java, R.layout.item_search_result) {
    override fun onBindViewHolder(holder: KtxHolder, item: SearchResult, payloads: List<Any>) {
        holder.text.text = item.value
        holder.itemView.setOnClickListener { listener.onSearchResultClick(item) }
    }

    interface Listener {
        fun onSearchResultClick(item: SearchResult)
    }

}