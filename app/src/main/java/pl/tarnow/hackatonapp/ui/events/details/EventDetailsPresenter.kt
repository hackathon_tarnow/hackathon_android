package pl.tarnow.hackatonapp.ui.events.details

import io.reactivex.android.schedulers.AndroidSchedulers
import org.threeten.bp.LocalDateTime
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.api.EventDetails
import pl.tarnow.hackatonapp.ui.Presenter
import timber.log.Timber

interface EvensDetailsView {
    fun showEvents(imageUrl: String, name: String, date: LocalDateTime, details: String)
    fun showError()
    fun openMap(latitude: Double, longitude: Double)
}

class EventDetailsPresenter(
    private val api: Api,
    private val baseurl: String
) : Presenter<EvensDetailsView>() {

    private var detailsCache: EventDetails? = null

    fun loadEventDetails(id: Long) {
        api.getEventDetails(id)
            .doOnSuccess { detailsCache = it }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { details ->
                    view.showEvents(
                        imageUrl = "$baseurl/files/${details.fileName}",
                        name = details.name,
                        date = details.eventDate,
                        details = details.eventDescription ?: ""
                    )
                },
                { error ->
                    Timber.e(error)
                    view.showError()
                }
            )
            .also { disposables.add(it) }
    }

    fun onMapFabClick() {
        detailsCache?.run {
            view.openMap(latitude ?: 0.0, longitude ?: 0.0)
        }
    }
}