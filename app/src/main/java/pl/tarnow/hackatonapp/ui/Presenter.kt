package pl.tarnow.hackatonapp.ui

import io.reactivex.disposables.CompositeDisposable

open class Presenter<V : Any> {
    protected lateinit var view: V
    protected val disposables = CompositeDisposable()

    open fun onAttach(view: V) {
        this.view = view
    }

    open fun onDetach() {
        disposables.clear()
    }
}