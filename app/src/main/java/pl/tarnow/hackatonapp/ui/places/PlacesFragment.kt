package pl.tarnow.hackatonapp.ui.places


import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.common.Image
import com.here.android.mpa.common.OnEngineInitListener
import com.here.android.mpa.common.ViewObject
import com.here.android.mpa.mapping.MapGesture
import com.here.android.mpa.mapping.MapMarker
import com.here.android.mpa.mapping.MapObject

import pl.tarnow.hackatonapp.R
import com.here.android.mpa.mapping.SupportMapFragment
import kotlinx.android.synthetic.main.fragment_places.*
import org.koin.android.ext.android.inject
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.api.Place
import timber.log.Timber


class PlacesFragment : Fragment(), OnEngineInitListener, PlacesView {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment_places
        return inflater.inflate(R.layout.fragment_places, container, false)
    }

    private val selectedId : Long? by lazy {
        val rawId = arguments!!.getLong("selectedId")
        if(rawId <= 0) null else rawId
    }
    private val api: Api by inject()
    private val presenter = PlacesPresenter(api)
    private val currentMarkers: MutableList<MapMarker> = mutableListOf()

    private lateinit var mapFragment: SupportMapFragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener {
            NavHostFragment.findNavController(this).popBackStack()
        }

        mapFragment = childFragmentManager.findFragmentById(R.id.mapFragmentView) as SupportMapFragment
        mapFragment.init(this)
    }

    private lateinit var map: com.here.android.mpa.mapping.Map

    override fun onEngineInitializationCompleted(error: OnEngineInitListener.Error) {
        if (error === OnEngineInitListener.Error.NONE) {
            // retrieve a reference of the map from the map fragment
            map = mapFragment.map
            // Set the map center to the Vancouver region (no animation)
            map.setCenter(
                GeoCoordinate(50.01381, 20.98698, 0.0),
                com.here.android.mpa.mapping.Map.Animation.NONE
            )
            // Set the zoom level to the average between min and max
            map.zoomLevel = (map.maxZoomLevel + map.minZoomLevel) / 2

            val listener = object : MapGesture.OnGestureListener.OnGestureListenerAdapter() {
                override fun onMapObjectsSelected(objects: MutableList<ViewObject>): Boolean {
                    objects
                        .asSequence()
                        .filter { it.baseType == ViewObject.Type.USER_OBJECT }
                        .map { it as MapObject }
                        .filter { it.type == MapObject.Type.MARKER }
                        .map { it as MapMarker }
                        .take(1)
                        .toList()
                        .forEach {
                            presenter.onPlaceClick(it.title.toLong())
                        }

                    return true
                }
            }

            mapFragment.mapGesture.addOnGestureListener(listener)

            presenter.loadPlaces(selectedId)
        } else {
            System.out.println("ERROR: Cannot initialize Map Fragment, cause ${error.details} ${error.throwable}")
        }
    }

    override fun showPlacesOnMap(places: List<Place>, selected: Place?) {
        Timber.i("$places")

        map.removeMapObjects(currentMarkers as List<MapObject>)
        currentMarkers.clear()

        for (place in places) {
            val marker = MapMarker(GeoCoordinate(place.latitude?:0.0, place.longitude?:0.0)).apply {
                this.title = place.id.toString()
                this.icon = Image().apply {
                    if (place.id == selected?.id) {
                        setImageResource(R.drawable.ic_place_selected)
                    } else {
                        setImageResource(R.drawable.ic_place)
                    }
                }
            }
            currentMarkers.add(marker)
            map.addMapObject(marker)
        }

        if (selected != null) {
            showPlaceDetails(selected)
        }
    }

    private fun showPlaceDetails(place: Place) {
        Timber.i("details: $place")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(placesLayout)
        }
        details.visibility = View.VISIBLE
        name.text = place.name
        description.text = place.content
    }

    override fun showError() {
        Toast.makeText(context, getString(R.string.generic_error), Toast.LENGTH_LONG).show()
    }

    override fun onStart() {
        super.onStart()
        presenter.onAttach(this)
    }

    override fun onStop() {
        presenter.onDetach()
        super.onStop()
    }
}
