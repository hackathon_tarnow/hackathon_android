package pl.tarnow.hackatonapp.ui.places

import io.reactivex.android.schedulers.AndroidSchedulers
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.api.Place
import pl.tarnow.hackatonapp.ui.Presenter
import timber.log.Timber

interface PlacesView {
    fun showPlacesOnMap(places: List<Place>, selected: Place?)
    fun showError()
}

class PlacesPresenter(
    private val api: Api
) : Presenter<PlacesView>() {

    private var placesCache: List<Place> = emptyList()
    private var selected: Place? = null

    fun loadPlaces(selectedOnStartId: Long?) {
//                Single.just(listOf(
//            Place(0, "WC 2", "Lorem Ipsum DUPA", 50.01381, 20.98698),
//            Place(1, "WC", "Lorem Ipsum", 50.03381, 20.95698)
//        ))

        api.getPlaces()
            .doOnSuccess { placesCache = it }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { places ->
                    selected = places.firstOrNull { it.id == selectedOnStartId }
                    view.showPlacesOnMap(places, selected)
                },
                { error ->
                    Timber.e(error)
                    view.showError()
                }
            )
            .also { disposables.add(it) }
    }

    fun onPlaceClick(placeId: Long) {
        val place = placesCache.find { it.id == placeId }
        selected = place

        view.showPlacesOnMap(placesCache, selected)
    }

}