package pl.tarnow.hackatonapp.ui.search

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.api.SearchResult
import pl.tarnow.hackatonapp.ui.Presenter
import timber.log.Timber
import java.util.concurrent.TimeUnit

interface SearchView {
    fun showError()
    fun showResults(items: List<SearchResult>)
    fun setLoading(loading: Boolean)
}

class SearchPresenter(
    private val api: Api
) : Presenter<SearchView>() {

    private val querySubject: PublishSubject<String> = PublishSubject.create()


    fun onQueryChange(query: String) {
        querySubject.onNext(query)
    }

    override fun onAttach(view: SearchView) {
        super.onAttach(view)

        querySubject
            .debounce(400, TimeUnit.MILLISECONDS)
            .switchMapSingle { query ->

                Single.just(query)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSuccess { view.setLoading(true) }
                    .observeOn(Schedulers.io())
                    .flatMap { api.search(query) }
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError {
                        Timber.e(it)
                        view.showError()
                    }
                    .onErrorReturnItem(emptyList())
                    .doFinally { view.setLoading(false) }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { view.showResults(it) },
                {
                    Timber.e(it)
                    view.showError()
                }
            )
            .also { disposables.add(it) }
    }
}