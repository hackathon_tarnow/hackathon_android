package pl.tarnow.hackatonapp.ui.news.list

import io.reactivex.android.schedulers.AndroidSchedulers
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.ui.Presenter
import timber.log.Timber

interface NewsListView {
    fun showNews(news: List<NewsItem>)
    fun showError()
}

class NewsListPresenter(
    private val api: Api,
    private val baseurl: String
) : Presenter<NewsListView>() {

    fun loadNews() {

        api.getNews()
            .map { newsList ->
                newsList.map { news ->
                    NewsItem(
                        id = news.id,
                        name = news.title,
                        imageUrl = "$baseurl/files/${news.fileName}",
                        date = news.data
                    )
                }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { view.showNews(it) },
                {
                    Timber.e(it)
                    view.showError()
                }
            )
            .also { disposables.add(it) }
    }
}