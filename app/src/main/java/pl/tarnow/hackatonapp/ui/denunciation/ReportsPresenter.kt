package pl.tarnow.hackatonapp.ui.denunciation

import io.reactivex.android.schedulers.AndroidSchedulers
import org.threeten.bp.LocalDateTime
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.api.Report
import pl.tarnow.hackatonapp.ui.Presenter
import timber.log.Timber

interface ReportsListView {
    fun showReports(items: List<Report>)
    fun showError()
}

class ReportsListPresenter(
    private val api: Api) : Presenter<ReportsListView>(){
    fun loadReports() {
        api.getReports()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { view.showReports(it) },
                {
                    Timber.e(it)
                    view.showError()
                }
            )
            .also { disposables.add(it) }
    }
}