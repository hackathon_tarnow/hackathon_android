package pl.tarnow.hackatonapp.ui

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.View
import it.bienkowski.recyclerdelegates.delegates.BaseRecyclerDelegate
import kotlinx.android.extensions.LayoutContainer

class KtxHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer

abstract class KtxDelegate<I>(itemClass: Class<I>, @LayoutRes layoutResId: Int) :
    BaseRecyclerDelegate<I, KtxHolder>(itemClass, layoutResId) {
    override fun createViewHolder(view: View): KtxHolder {
        return KtxHolder(view)
    }
}