package pl.tarnow.hackatonapp.ui.denunciation

import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_report.*
import pl.tarnow.hackatonapp.R
import pl.tarnow.hackatonapp.api.Report
import pl.tarnow.hackatonapp.ui.KtxDelegate
import pl.tarnow.hackatonapp.ui.KtxHolder

class ReportDelegate(
    val baseUrl: String
) : KtxDelegate<Report>(Report::class.java, R.layout.item_report){
    override fun onBindViewHolder(holder: KtxHolder, item: Report, payloads: List<Any>) {
        holder.title.text = item.title
        holder.info.text = item.application
        Picasso.Builder(holder.thumbnail.context)
            .listener { _, _, e -> e.printStackTrace() }
            .build()
            .load("$baseUrl/files/${item.fileName}")
            .error(R.drawable.image_placholder)
            .fit()
            .into(holder.thumbnail)
    }
}
