package pl.tarnow.hackatonapp.ui.denunciation


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.fragment_denunciation.*
import org.koin.android.ext.android.inject

import pl.tarnow.hackatonapp.R
import pl.tarnow.hackatonapp.api.Api

class DenunciationFragment : Fragment(), DenunciationView {

    private val api: Api by inject()
    private val presenter = DenunciationPresenter(api)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_denunciation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener {
            NavHostFragment.findNavController(this).popBackStack()
        }

        send.setOnClickListener {
            presenter.send(title.text.toString(), info.text.toString())
        }
    }

    override fun showError() {
        Toast.makeText(context, getString(R.string.send_error), Toast.LENGTH_LONG).show()
    }

    override fun showSuccess() {
        Toast.makeText(context, getString(R.string.send_success), Toast.LENGTH_LONG).show()
        NavHostFragment.findNavController(this).popBackStack()
    }

    override fun onStart() {
        super.onStart()
        presenter.onAttach(this)
    }

    override fun onStop() {
        presenter.onDetach()
        super.onStop()
    }

}
