package pl.tarnow.hackatonapp.ui.denunciation


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import it.bienkowski.recyclerdelegates.DelegatingRecyclerAdapter
import it.bienkowski.recyclerdelegates.managers.SimpleDelegateManager
import kotlinx.android.synthetic.main.fragment_reports_list.*
import org.koin.android.ext.android.inject
import org.koin.core.qualifier.named

import pl.tarnow.hackatonapp.R
import pl.tarnow.hackatonapp.api.Api
import pl.tarnow.hackatonapp.api.Report
import pl.tarnow.hackatonapp.ui.news.list.NewsDelegate
import pl.tarnow.hackatonapp.ui.news.list.NewsItem

class ReportsListFragment : Fragment(), ReportsListView, NewsDelegate.Listener {


    private val api: Api by inject()
    private val baseurl: String by inject(named("BASEURL"))
    private val presenter = ReportsListPresenter(api)

    private val adapter = DelegatingRecyclerAdapter<Any>(
        SimpleDelegateManager.withDelegates(
            ReportDelegate(baseurl)
        )
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reports_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener {
            NavHostFragment.findNavController(this).popBackStack()
        }

        addFab.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_reportsListFragment_to_denunciationFragment)
        }

        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }

    override fun showReports(items: List<Report>) {
        adapter.submitList(items)
    }

    override fun showError() {
        Toast.makeText(context, getString(R.string.generic_error), Toast.LENGTH_LONG).show()
    }

    override fun onStart() {
        super.onStart()
        presenter.onAttach(this)
        presenter.loadReports()
    }

    override fun onStop() {
        presenter.onDetach()
        super.onStop()
    }

    override fun onNewsClick(item: NewsItem) {

    }
}
