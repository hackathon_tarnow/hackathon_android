package pl.tarnow.hackatonapp.api

import android.preference.PreferenceManager
import com.squareup.moshi.Moshi
import io.reactivex.schedulers.Schedulers
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

val apiModule = module {

    single(named("BASEURL")) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(get())
        if (prefs.contains("url")) {
            prefs.getString("url", "")
        } else {
            "http://149.202.63.144:8080"
        }
    }

    single {
        val moshi = Moshi.Builder()
            .add(LocalDateTimeAdapter())
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(get<String>(named("BASEURL")))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()

        retrofit.create(Api::class.java)
    }
}