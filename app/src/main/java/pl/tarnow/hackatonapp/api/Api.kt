package pl.tarnow.hackatonapp.api

import io.reactivex.Single
import okhttp3.ResponseBody
import org.threeten.bp.LocalDateTime
import retrofit2.Response
import retrofit2.http.*

data class Place(
    val id: Long,
    val name: String,
    val content: String,
    val latitude: Double?,
    val longitude: Double?
)

data class News(
    val id: Long,
    val title: String,
    val fileName: String?,
    val data: LocalDateTime
)

data class NewsDetails(
    val id: Long,
    val title: String,
    val text: String?,
    val fileName: String,
    val data: LocalDateTime
)

data class Event(
    val id: Long,
    val fileName: String?,
    val name: String,
    val eventDate: LocalDateTime
)

data class EventDetails(
    val id: Long,
    val fileName: String?,
    val latitude: Double?,
    val longitude: Double?,
    val name: String,
    val eventDescription: String?,
    val eventDate: LocalDateTime
)

data class PostReportBody(
    val title: String,
    val application: String,
    val latitude: Double? = null,
    val longitude: Double? = null
)

data class Report(
    val id: Long,
    val title: String,
    val application: String,
    val latitude: Double? = null,
    val longitude: Double? = null,
    val fileName: String? = null
)

data class SearchResult(
    val id: Long,
    val objectId: Long,
    val type: String,
    val value: String
)

interface Api {

    @GET("places")
    fun getPlaces(): Single<List<Place>>

    @GET("news")
    fun getNews(): Single<List<News>>

    @GET("news/{id}")
    fun getNewsDetails(@Path("id") id: Long): Single<NewsDetails>

    @GET("events")
    fun getEvents(): Single<List<Event>>

    @GET("events/{id}")
    fun getEventDetails(@Path("id") id: Long): Single<EventDetails>

    @POST("reports")
    fun postReport(@Body report: PostReportBody) : Single<Response<ResponseBody>>

    @GET("reports")
    fun getReports(): Single<List<Report>>

    @GET("search")
    fun search(@Query("search") query : String) : Single<List<SearchResult>>
}