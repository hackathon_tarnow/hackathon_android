package pl.tarnow.hackatonapp.api

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

class LocalDateTimeAdapter {
    private val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")

    @FromJson
    fun fromJson(raw: String) : LocalDateTime {
        return LocalDateTime.parse(raw, dateFormatter)
    }

    @ToJson
    fun toJson(value: LocalDateTime): String {
        return value.format(dateFormatter)
    }
}